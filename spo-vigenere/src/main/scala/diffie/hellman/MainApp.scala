package diffie.hellman

import java.io.{File, PrintWriter}

import org.github.marklister.basen.BaseN._
import org.github.marklister.basen.Binary
import org.slf4j.{Logger, LoggerFactory}
import diffie.hellman.MainApp._

import scala.io.Source
import scala.util.{Failure, Random, Success, Try}

object MainApp {
  type ErrorMsg = String
  type AlphabetChar = Char
  type SecretKey = Seq[Binary]
  type EncryptedText = Seq[Binary]
  type DecryptedText = Seq[Char]
  
  
  def main(args: Array[String]): Unit = {
    if (args.length > 1) {
      val sourceText = args(0)
      val resultDir = args(1)
      
      val app = new MainApp
      app.readSourceFile(sourceText).right.flatMap { text =>
        app.generateSecretKey(text.length).right.flatMap { secretKey =>
          app.encryptText(text, secretKey, resultDir).right.flatMap { encText =>
            app.decryptText(encText, secretKey, resultDir)
          }
        }
      } match {
        case Right(_) => println("Task executed correctly")
        case Left(error) => println(error)
      }
    } else println("Not enough arguments")
  }
}

class MainApp {
  
  private[this] val logger: Logger = LoggerFactory.getLogger(this.getClass)
  private[this] val alphabet = 1 to 255
  private[this] val secretWord = "dog"
  private[this] val alphabetMap: Map[AlphabetChar, Binary] = createAlphabet()
  
  def readSourceFile(source: String): Either[ErrorMsg, Seq[Char]] = {
    Try(Right(Source.fromResource(source).getLines.toVector.mkString.map(_.toLower).toSeq))
      .getOrElse(Left("Error parsing source file"))
  }
  
  def createAlphabet(): Map[AlphabetChar, Binary] = {
    alphabet.indices.map(num => alphabet(num).toChar -> num.toString.d.b).toMap
  }
  
  def generateSecretKey(textSize: Int): Either[ErrorMsg, SecretKey] = {
    Try {
      if (secretWord.length >= textSize) secretWord.take(textSize).map(alphabetMap(_))
      else {
        var secretKey = ""
        while (secretKey.length < textSize) secretKey += secretWord
        secretKey.take(textSize).map(alphabetMap(_))
      }
    } match {
      case Success(key) => Right(key)
      case Failure(ex) => Left(ex.getMessage)
    }
  }
  
  def encryptText(text: Seq[Char], secretKey: SecretKey, resultDir: String): Either[ErrorMsg, EncryptedText] = {
    Try(new PrintWriter(new File(resultDir, "outputEnc.txt"))) match {
      case Success(pw) =>
        val encText = text.map(c => alphabetMap(c)).zipWithIndex
          .map { case (byte, i) => (byte xor secretKey(i)).b }
        pw.write(encText.map(_.underlying).mkString("\t"))
        logger.debug("Encrypted text was successfully written to output file")
        pw.close()
        Right(encText)
      case Failure(ex) =>
        logger.error(ex.getMessage)
        Left("Can't write encrypted text to output file")
    }
  }
  
  def decryptText(encText: Seq[Binary], secretKey: SecretKey, resultDir: String): Either[ErrorMsg, DecryptedText] = {
    Try(new PrintWriter(new File(resultDir, "outputDec.txt"))) match {
      case Success(pw) =>
        val decText = encText.zipWithIndex.map { case (byte, i) => (byte xor secretKey(i)).b }
          .map(byte => alphabetMap.find(_._2.underlying == byte.underlying).get._1)
        pw.write(decText.mkString)
        logger.debug("Decrypted text was successfully written to decrypted file!")
        pw.close()
        Right(decText)
      case Failure(ex) =>
        logger.error(ex.getMessage)
        Left("Can't write encrypted text to output file")
    }
  }
  
}
