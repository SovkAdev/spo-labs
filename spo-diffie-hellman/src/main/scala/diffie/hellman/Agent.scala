package diffie.hellman

import java.io.{File, PrintWriter}

import scala.io.Source
import scala.math._
import scala.util.{Failure, Random, Success, Try}

class Agent(name: String) {
  
  type ErrorMsg = String
  type PublicKey = Double
  type PrivateKey = Double
  type Encryptor = Double
  
  private[this] object Config {
    val module = 17
    val root = 3 //первообразный корень g по модулю n - число такое, что все его степени по модулю n взаимнопростые с n)
    val privateKey: PrivateKey = Random.nextInt(10)
    var publicKey: Option[PublicKey] = None
    var encryptor: Option[Encryptor] = None
  }
  
  
  def writePublicKeyToFile(resultDir: String): Either[ErrorMsg, Unit] = {
    Try(new PrintWriter(new File(resultDir, "public_key.txt"))) match {
      case Success(pw) =>
        val pk = (pow(Config.root, Config.privateKey) % Config.module).toString
        println(s"$name wrote publicKey - $pk to file ")
        pw.write(pk)
        pw.close()
        Right(())
      case Failure(ex) => Left(ex.getMessage)
    }
  }
  
  def getPublicKeyFromFile(resultDir: String): Either[ErrorMsg, Unit] = {
    Try(Source.fromFile(new File(resultDir, "public_key.txt")).getLines().toVector.mkString) match {
      case Success(publicKey) =>
        println(s"$name got publicKey - $publicKey from file ")
        Config.publicKey = Some(publicKey.toDouble)
        Right(())
      case Failure(ex) => Left(ex.getMessage)
    }
  }
  
  def createEncryptor(): Either[ErrorMsg, Unit] = {
    if (Config.publicKey.isEmpty) Left("Public key doesn't exists")
    else {
      println()
      println(s"---------Start generate encryptor for agent $name----------------")
      println(s"Public key: ${Config.publicKey.get}")
      println(s"Private key: ${Config.privateKey}")
      Config.encryptor = Some(pow(Config.publicKey.get, Config.privateKey) % Config.module)
      println(s"Encryptor: ${Config.encryptor.get}")
      println()
      Right(())
    }
  }
  
  def getEncryptor: Either[ErrorMsg, Encryptor] = {
    if (Config.publicKey.isEmpty) Left("Encryptor doesnt' exists")
    else Right(Config.encryptor.get)
  }
}
