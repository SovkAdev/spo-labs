package diffie.hellman

object MainApp {
  
  def main(args: Array[String]): Unit = {
    if (args.length > 0) {
      val resultDir = args(0)
      
      val Alice = new Agent("Alice")
      val Bob = new Agent("Bob")
      Alice.writePublicKeyToFile(resultDir).right.flatMap { _ =>
        Bob.getPublicKeyFromFile(resultDir).right.flatMap { _ =>
          Bob.writePublicKeyToFile(resultDir).right.flatMap { _ =>
            Alice.getPublicKeyFromFile(resultDir)
          }
        }
      } match {
        case Right(_) =>
          Set(Alice, Bob).foreach(_.createEncryptor())
          println(s"Alice encryptor: ${Alice.getEncryptor}")
          println(s"Bob encryptor: ${Bob.getEncryptor}")
        case Left(error) => println(error)
      }
      
    } else println("Not enough arguments")
  }
}

