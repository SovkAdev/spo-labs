package rsa

import org.slf4j.{Logger, LoggerFactory}
import rsa.KeyGen.{PrivateKey, PublicKey}

import scala.util.Random

object KeyGen {
  
  case class PublicKey(e: BigInt, n: BigInt)
  
  case class PrivateKey(d: BigInt, n: BigInt)
  
}

class KeyGen {
  
  private[this] val logger: Logger = LoggerFactory.getLogger(this.getClass)
  
  def generateKeys(): (PublicKey, PrivateKey) = {
    val p = generatePrimeNum() //1 простое число
    val q = generatePrimeNum() //2 простое число
    val n = p * q
    val eler: BigInt = (p - 1) * (q - 1) // функция Эйлера
    val d = generateD(eler) //число взаимнопростое с фун. Эйлера (p-1)(q-1)
    val e = generateE(d, eler) // число e с условием e*d mod (p-1)(q-1) = 1
    
    logger.debug(s"p = $p\tq=$q\tn=$n\teler = $eler\td=$d\te=$e")
    (PublicKey(e, n), PrivateKey(d, n))
  }
  
  private[this] def generatePrimeNum(): BigInt = {
    def primeStream(s: Stream[Int]): Stream[Int] =
      Stream.cons(s.head, primeStream(s.tail.filter(_ % s.head != 0)))
    
    primeStream(Stream.from(2)).take(100)(Random.nextInt(100))
  }
  
  private[this] def generateD(eler: BigInt): BigInt = {
    Stream.from(2).dropWhile(BigInt(_).gcd(eler) != 1).head
  }
  
  private[this] def generateE(d: BigInt, eler: BigInt): Int = {
    Stream.from(2).dropWhile(_ * d % eler != 1).head
  }
}
