package rsa

import org.slf4j.{Logger, LoggerFactory}

import scala.math._

object MainApp {
  def main(args: Array[String]): Unit = {
    val logger: Logger = LoggerFactory.getLogger(this.getClass)
    val message = "Secret message which we want to encrypt and then decrypt with rsa algorithm"
    
    val (publicKey, privateKey) = new KeyGen().generateKeys()
    
    val encryptedMes: Seq[BigInt] = message.map(BigInt(_)).map(c => c.modPow(publicKey.e, publicKey.n))
    val decryptedMes: Seq[BigInt] = encryptedMes.map(c => c.modPow(privateKey.d, privateKey.n))
    logger.debug(s"\nMessage:\t\t${message.map(_.toInt)}\nEncryptedMes:\t$encryptedMes\nDecryptedMes:\t$decryptedMes")
  }
}

