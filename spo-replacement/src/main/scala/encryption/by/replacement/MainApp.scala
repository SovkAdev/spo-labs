package encryption.by.replacement

import java.io._

import encryption.by.replacement.MainApp._
import org.slf4j.{Logger, LoggerFactory}

import scala.annotation.tailrec
import scala.io.Source
import scala.util.{Failure, Random, Success, Try}

object MainApp {
  
  type ErrorMsg = String
  type AlphabetChar = Char
  type SecAlphabetChar = Char
  type EncryptedText = Seq[Char]
  type DecryptedText = Seq[Char]
  
  def main(args: Array[String]): Unit = {
    if (args.length > 1) {
      val sourceText = args(0)
      val resultDir = args(1)
      
      val app = new MainApp
      app.readSourceFile(sourceText).right.flatMap { text =>
        app.encryptText(text, resultDir).right.flatMap { encText =>
          app.decryptText(encText, resultDir)
        }
      } match {
        case Right(_) => println("Task executed correctly")
        case Left(error) => println(error)
      }
    } else println("Not enough arguments")
    
  }
}

class MainApp {
  
  private[this] val logger: Logger = LoggerFactory.getLogger(this.getClass)
  private[this] val alphabet: Seq[Char] = 'a' to 'z'
  private[this] val secCharsCount: Int = 4
  private[this] val alphabetMap: Map[AlphabetChar, SecAlphabetChar] = generateAlphabets()
  
  def readSourceFile(source: String): Either[ErrorMsg, Seq[Char]] = {
    Try(Right(Source.fromResource(source).getLines.toVector.mkString.map(_.toLower).toSeq))
      .getOrElse(Left("Error parsing source file"))
  }
  
  @tailrec
  private[this] def generateAlphabets(): Map[Char, Char] = {
    var secretAlphabet = Random.alphanumeric.filter(_.isLetter).take(secCharsCount).map(_.toLower).toSet.toSeq
    secretAlphabet.size match {
      case this.secCharsCount =>
        alphabet.filterNot(secretAlphabet.contains).foreach(secretAlphabet :+= _)
        alphabet.zipWithIndex.map { case (c, i) => c -> secretAlphabet(i) }.toMap
      case _ => generateAlphabets()
    }
  }
  
  def encryptText(text: Seq[Char], resultDir: String): Either[ErrorMsg, EncryptedText] = {
    Try(new PrintWriter(new File(resultDir, "outputEnc.txt"))) match {
      case Success(pw) =>
        val encText = text.map(c => if (alphabet.contains(c)) alphabetMap(c) else c)
        pw.write(encText.mkString)
        logger.debug("Encrypted text was successfully written to output file")
        pw.close()
        Right(encText)
      case Failure(ex) =>
        logger.error(ex.getMessage)
        Left("Can't write encrypted text to output file")
    }
  }
  
  def decryptText(encText: Seq[Char], resultDir: String): Either[ErrorMsg, DecryptedText] = {
    val correctMas = Seq('e', 't', 'a', 'o', 'n', 'r',
      'i', 's', 'h', 'd', 'l', 'f', 'c', 'm', 'u', 'g',
      'y', 'p', 'w', 'b', 'v', 'k', 'x', 'j', 'q', 'z')
    
    var countMas = alphabet.map(c => c -> 0).toMap
    
    encText.filter(alphabet.contains).foreach(c => countMas = countMas.updated(c, countMas(c) + 1))
    
    val correctMap = countMas.toSeq.sortBy(_._2).reverse
      .zipWithIndex.map { case ((c, _), i) => c -> correctMas(i) }.toMap
    
    Try(new PrintWriter(new File(resultDir, "outputDec.txt"))) match {
      case Success(pw) =>
        val decText = encText.map(c => if (correctMap.contains(c)) correctMap(c) else c)
        pw.write(decText.mkString)
        logger.debug("Decrypted text was successfully written to decrypted file!")
        pw.close()
        Right(decText)
      case Failure(ex) =>
        logger.error(ex.getMessage)
        Left("Can't write encrypted text to output file")
    }
  }
}
