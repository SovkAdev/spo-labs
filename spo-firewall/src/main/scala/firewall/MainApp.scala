package firewall

import java.util.concurrent.TimeoutException

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.io.Source
import scala.util.matching.Regex

object MainApp {
  def main(args: Array[String]): Unit = {
    val app = new MainApp
    app.readFileWithTimerLimit(1000, "someText.txt")
    app.analyzer()
    app.correction()
    println(app.restoreSourceText())
  }
}

class MainApp {
  var text: Seq[String] = Seq.empty[String]
  var correctWords: Seq[(Int, String)] = Seq.empty[(Int, String)]
  var incorrectWords: Seq[(Int, String)] = Seq.empty[(Int, String)]

  val correctWord: Regex = "[a-zA-Zа-яА-Я]+[,!?.]?".r
  val correctDigit: Regex = "[0-9]+[,!?.]?".r

  def readFileWithTimerLimit(timer: Long, filename: String): Unit = {
    val task = Future(for (line <- Source.fromResource(filename).getLines) text :+= line)
    try Await.ready(task, timer.milliseconds) catch {
      case _: TimeoutException => println("Only part of file was read")
    }
  }

  def analyzer(): Unit = {
    if (text.nonEmpty) {
      val words = text.mkString.split(" ")
      words.zipWithIndex.foreach { case (word, id) => checkCorrection(id, word) }
    }
  }

  def checkCorrection(id: Int, word: String): Unit = {
    word match {
      case correctWord(_*) => correctWords :+= (id, word)
      case correctDigit(_*) => correctWords :+= (id, word)
      case _ => incorrectWords :+= (id, word)
    }
  }

  def correction(): Unit = {
    incorrectWords.foreach {
      case (id, word) => correctWords :+= (id, correctWord.findFirstIn(word.filterNot(_.isDigit)).get)

    }
  }

  def restoreSourceText(): String = {
    correctWords.sortBy(_._1).map(_._2).mkString(" ")
  }

}
